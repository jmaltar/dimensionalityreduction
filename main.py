from dataset import DatasetUSPSSingle, DatasetUSPS
from usps import get_USPS_data
import torch
import torch.optim
from torch.utils.data import DataLoader
from net import get_model
from losses import ContrastiveLossSingle, MMDSLossSingle, ContrastiveLoss, MMDSLoss
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import numpy as np
from visualization import visualize
from pathlib import Path
import os


def train(dataset, model, num_epochs, criterion, optimizer, scheduler, name, save_freq=10, log=False, log_callb=None, destination_folder="./weights/", train_procedure="Single"):
    log_str = ""
    model.train()

    dataloader = None
    if train_procedure == "Multiple":
        dataloader = DataLoader(dataset, batch_size=4096, shuffle=True, num_workers=0)

    for epoch_i in range(num_epochs):
        epoch_loss = 0.0

        if train_procedure == "Single":
            dataloader = DataLoader(dataset, batch_size=64, shuffle=True, num_workers=0)

        for i, batch in enumerate(dataloader):
            if train_procedure == "Single":

                x_original_batch = batch[0]
                y_batch = batch[1]
                optimizer.zero_grad()
                with torch.set_grad_enabled(True):
                    x_batch = model(x_original_batch)
                    if type(criterion).__name__ == "ContrastiveLossSingle":
                        loss = criterion(x_batch, y_batch)
                    elif type(criterion).__name__ == "MMDSLossSingle":
                        loss = criterion(x_original_batch, x_batch)
                    loss.backward()
                    optimizer.step()

            elif train_procedure == "Multiple":
                x_i_original_batch = batch[0]
                x_j_original_batch = batch[1]
                y_i_batch = batch[2]
                y_j_batch = batch[3]
                optimizer.zero_grad()

                # True if training, else False
                with torch.set_grad_enabled(True):
                    x_i_batch = model(x_i_original_batch)
                    x_j_batch = model(x_j_original_batch)
                    if type(criterion).__name__ == "ContrastiveLoss":
                        loss = criterion(x_i_batch, x_j_batch, y_i_batch, y_j_batch)
                    elif type(criterion).__name__ == "MMDSLoss":
                        loss = criterion(x_i_original_batch, x_j_original_batch, x_i_batch, x_j_batch)
                    # if training
                    loss.backward()
                    optimizer.step()

            epoch_loss += loss.item()

        info_str = "Epoch {}/{}; Loss: {}".format(epoch_i + 1, num_epochs, epoch_loss)
        #print(info_str)
        if log:
            log_str += "{}\n".format(info_str)
        if (epoch_i + 1) % save_freq == 0:
            torch.save(model.state_dict(), "{}{}.pt".format(destination_folder, "{}_it_{}".format(name, epoch_i + 1)))
        scheduler.step()
    if log:
        log_callb(log_str)


def train_experiment(
        training_nr=0,
        model_name="LeNet5",
        criterion_name="ContrastiveLoss",
        dataset_name="USPS",
        num_classes=3,
        optimizer_name="SGD",
        num_epochs=100,
        lr=0.0001,
        weight_decay=1e-1,
        scheduler_step_size=10,
        scheduler_gamma=0.5,
        to_cuda=True,
        save_freq=5,
        log=False,
        destination_folder="./weights/",
        train_procedure="Single"
        ):
    log_str = ""
    name = "{}_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}".format(training_nr, model_name, criterion_name, dataset_name, num_classes, optimizer_name, num_epochs, lr, weight_decay, scheduler_step_size, scheduler_gamma)
    if log:
        log_str += "{}\n".format(name)
    dataset = None
    if dataset_name == "USPS":
        x, y = get_USPS_data()
        up_sample = "SimpleNet" not in model_name
        if train_procedure == "Single":
            dataset = DatasetUSPSSingle(x, y, up_sample=up_sample, to_cuda=to_cuda)
        elif train_procedure == "Multiple":
            dataset = DatasetUSPS(x, y, up_sample=up_sample, to_cuda=to_cuda)

    model = get_model(model_name, num_classes)

    if to_cuda:
        model = model.to(torch.device("cuda:0"))

    criterion = None
    if criterion_name == "ContrastiveLoss":
        if train_procedure == "Single":
            criterion = ContrastiveLossSingle()
        elif train_procedure == "Multiple":
            criterion = ContrastiveLoss()
    elif criterion_name == "MMDSLoss":
        if train_procedure == "Single":
            criterion = MMDSLossSingle()
        elif train_procedure == "Multiple":
            criterion = MMDSLoss()

    optimizer = None
    if optimizer_name == "SGD":
        optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=weight_decay)
    elif optimizer_name == "Adam":
        optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=scheduler_step_size, gamma=scheduler_gamma)

    Path(destination_folder).mkdir(parents=True, exist_ok=True)

    def log_callb(log_str):
        with open("./results/logs/{}.out".format(name), "w") as log_file:
            log_file.write(log_str)
        print(log_str)

    train(dataset, model, num_epochs, criterion, optimizer, scheduler, name, save_freq=save_freq, log=log, log_callb=log_callb, destination_folder=destination_folder, train_procedure=train_procedure)


def test_qualitatively(model_weights_path, data_type="test", num_samples=1000, save=False, save_dest="", gui_callb=None, dataset_name=None):
    model_name = model_weights_path.split("/")[-1].split("_")[1]

    dataset = None

    if dataset_name is None:
        dataset_name = model_weights_path.split("/")[-1].split("_")[3]

    if dataset_name == "MNIST":
        resize_factor = 32 if "SimpleNet" not in model_name else 16
        dataset = datasets.MNIST("./data/mnist/{}/".format(data_type), download=True, train=data_type == "train", transform=transforms.Compose([transforms.Resize(resize_factor), transforms.ToTensor()]))
    elif dataset_name == "USPS":
        x, y = get_USPS_data(data_type=data_type, num_samples_subset=num_samples)
        up_sample = "SimpleNet" not in model_name
        dataset = DatasetUSPSSingle(x, y, up_sample=up_sample)

    sampler = None
    if dataset_name == "MNIST":
        train_indices = torch.from_numpy(np.random.choice(len(dataset), size=(num_samples,), replace=False))
        sampler = torch.utils.data.SubsetRandomSampler(train_indices)

    dataloader = DataLoader(dataset, batch_size=128, shuffle=False, num_workers=0, sampler=sampler)
    num_classes = 3

    model = get_model(model_name, num_classes)
    model.load_state_dict(torch.load(model_weights_path))

    to_svg = True

    if model_weights_path.split("/")[-1][3] != dataset_name:
        mwp = model_weights_path.split("/")
        temp = mwp[-1].split("_")
        temp[3] = dataset_name
        mwp[-1] = "_".join(temp)
        model_weights_path = "/".join(mwp)
        print(model_weights_path)

    if save and save_dest == "":
        save_dest = "./results/images/{}_{}.{}".format(model_weights_path.split("/")[-1].replace(".pt", ""), data_type, "svg" if to_svg else "png")

    if gui_callb is None:
        visualize(model, dataloader, False, save, save_dest, to_svg=to_svg)
    else:
        gui_callb(model, dataloader)


if __name__ == '__main__':
    """
    training_nr = 0
    for num_classes in [3]:
        for model_name in ["LeNet5", "SimpleNet", "SimpleNet2", "SimpleNet3"]:
            for optimizer_name in ["SGD", "Adam"]:
                for lr in [0.05, 0.01, 0.005, 0.001]:
                    for weight_decay in [0, 0.01, 0.1, 0.2, 0.3, 0.4]:
                        for scheduler_step_size in [5, 10, 15, 20]:
                            for scheduler_gamma in [0.1, 0.5]:
                                training_nr += 1
                                train_experiment(train_procedure="Single", training_nr=training_nr, model_name=model_name, criterion_name="ContrastiveLoss",
                                           dataset_name="USPS", num_classes=num_classes, optimizer_name=optimizer_name, num_epochs=200,
                                           lr=lr, weight_decay=weight_decay, scheduler_step_size=scheduler_step_size, scheduler_gamma=scheduler_gamma,
                                           to_cuda=True, save_freq=10, log=True, destination_folder="./weights/contrastive_new/")
    """
    """
    training_nr = 1536
    for num_classes in [3]:
        for optimizer_name in ["SGD"]:
            for lr in [0.01, 0.005, 0.001]:
                for weight_decay in [0, 0.1, 0.4]:
                    for scheduler_step_size in [10, 20]:
                        for scheduler_gamma in [0.1]:
                            for model_name in ["LeNet5", "SimpleNet3"]:
                                training_nr += 1
                                train_experiment(train_procedure="Multiple", training_nr=training_nr, model_name=model_name, criterion_name="ContrastiveLoss",
                                           dataset_name="USPS", num_classes=num_classes, optimizer_name=optimizer_name, num_epochs=100,
                                           lr=lr, weight_decay=weight_decay, scheduler_step_size=scheduler_step_size, scheduler_gamma=scheduler_gamma,
                                           to_cuda=True, save_freq=10, log=True, destination_folder="./weights/contrastive/")

    """
    """
    for filepath in os.listdir("../contrastive/"):
        iteration_nr = int(filepath.split("_")[-1].replace(".pt", ""))
        if iteration_nr in [1, 10]:
            test_qualitatively("../contrastive/" + filepath, save=True, data_type="test", dataset_name="MNIST")
            test_qualitatively("../contrastive/" + filepath, save=True, data_type="train", dataset_name="USPS")
            test_qualitatively("../contrastive/" + filepath, save=True, data_type="test", dataset_name="USPS")
            #test_qualitatively("../contrastive/" + filepath, save=True, data_type="train", dataset_name="MNIST")
    """


    train_experiment(train_procedure="Single", training_nr=2884, model_name="SimpleNet2", criterion_name="MMDSLoss",
                     dataset_name="USPS", num_classes=20, optimizer_name="Adam", num_epochs=1000,
                     lr=0.01, weight_decay=0, scheduler_step_size=50, scheduler_gamma=0.1,
                     to_cuda=True, save_freq=100, log=False, destination_folder="./weights/mmds/")
