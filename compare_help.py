
def get_lines(filepath):
    lines = []
    with open(filepath) as file:
        line = file.readline()
        lines.append(line)
        while line:
            line = file.readline()
            lines.append(line)
    return lines


def nice_info(filename):
    model_name = filename.split("_")[1]
    loss = filename.split("_")[2]
    num_classes = filename.split("_")[4]
    optimizer = filename.split("_")[5]
    num_epochs = filename.split("_")[6]
    lr = filename.split("_")[7]
    weight_decay = filename.split("_")[8]
    scheduler_step_size = filename.split("_")[9]
    scheduler_gamma = filename.split("_")[10]
    epoch_nr = filename.split("_")[12]
    info = "Model: {}, Loss: {}, Num. classes: {}, optim.: {}, Nr. of epochs: {}, Learning rate: {}, Weight decay: {}, Scheduler step size: {}, Scheduler gamma: {}, Epoch nr.: {}".format(
        model_name,
        loss,
        num_classes,
        optimizer,
        num_epochs,
        lr,
        weight_decay,
        scheduler_step_size,
        scheduler_gamma,
        epoch_nr
    )
    return info


def net_name_map(net):
    d = {
        "SimpleNet": "SN1",
        "SimpleNet2": "SN2",
        "SimpleNet3": "SN3"
    }
    return d[net]

def latex_table(results):
    s = "\\begin{tabular} { | c | c | c | c | c | c | c | c | c | } \\hline \n"
    s += "\t$d$ & Network & Optimizer & $lr$ & $wd$ & $s_{\\text{step}}$ & $s_{\\gamma}$ & Loss & Loss (sklearn)  \\\\ \\hline \n"

    for k, v in results.items():
        print("Num classes: {}".format(k))
        c = 0
        for i, el in enumerate(v):
            it = int(el[0].split("_")[-1].replace(".pt", ""))
            net = el[0].split("_")[1]
            d = k
            optimizer = el[0].split("_")[5]
            lr = el[0].split("_")[7]
            wd = el[0].split("_")[8]
            s_step = el[0].split("_")[9]
            s_gamma = el[0].split("_")[10]
            print(el[1])
            if it == 100:
                c += 1
                if c == 1:
                    s += "\t\\multirow{" + str(num_of_results_for_each_out_dim) + "}{*}{" + str(d) + "}" \
                         + " & {\\ttfamily " + net_name_map(net) + "} & " + "{} & ${}$ & ${}$ & ${}$ & ${}$ & ${}$ &".format(optimizer, lr, wd, s_step, s_gamma, round(el[1], 2)) \
                         + "\\multirow{" + str(num_of_results_for_each_out_dim) + "}{*}{$" + str(round(el[2], 2)) + "$}" + "\\\\\n"
                else:
                    s += "\t\\cline{2-8} & {\\ttfamily " + net_name_map(net) + "} & " + "{} & ${}$ & ${}$ & ${}$ & ${}$ & ${}$ &".format(optimizer, lr, wd, s_step, s_gamma, round(el[1], 2)) + "\\\\\n"
            if c >= num_of_results_for_each_out_dim:
                s += "\t\\hline\n"
                break
    s += "\\end{tabular}\n"
    print(s)

if __name__ == '__main__':
    filepath = "./results/mmds/results_train.out"
    num_of_results_for_each_out_dim = 5
    lines = get_lines(filepath)

    d_results = {}

    for line in lines:
        data = line.split(" ")
        if data == ['']:
            continue

        sklearn_loss = float(data[-1].replace("\\n", ""))
        pytorch_loss = float(data[3].replace(";", ""))
        experiment_info = data[2][:-2]
        num_classes = int(experiment_info.split("_")[4])
        d_results.setdefault(num_classes, []).append((experiment_info, pytorch_loss, sklearn_loss))

    for k, v in d_results.items():
        d_results[k] = sorted(v, reverse=False, key= lambda x: x[1])

    latex_table(d_results)

