import sys
import time
import os
import subprocess
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QSlider, QApplication, QTreeWidgetItem, QGridLayout
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from visualization import visualize
from main import test_qualitatively
import threading

import matplotlib
matplotlib.use('TkAgg')

model_names = ["LeNet5", "SimpleNet", "SimpleNet2", "SimpleNet3"]
optimizer_names = ["SGD", "Adam"]
lrs = [0.05, 0.01, 0.005, 0.001]
weight_decays = [0, 0.01, 0.1, 0.2, 0.3, 0.4]
scheduler_step_sizes = [5, 10, 15, 20]
scheduler_gammas = [0.1, 0.5]
data_types = ["train", "test"]
epochs = list(range(10, 210, 10))
nums_samples = list(range(100, 1100, 100))

mapper = {
    "model_name": model_names,
    "optimizer_name": optimizer_names,
    "lr": lrs,
    "weight_decay": weight_decays,
    "scheduler_step_size": scheduler_step_sizes,
    "scheduler_gamma": scheduler_gammas,
    "data_type": data_types,
    "epoch": epochs,
    "num_samples": nums_samples
}


def training_nr(model_name_o, optimizer_name_o, lr_o, weight_decay_o, scheduler_step_size_o, scheduler_gamma_o):
    n = 0
    for model_name in ["LeNet5", "SimpleNet", "SimpleNet2", "SimpleNet3"]:
        for optimizer_name in ["SGD", "Adam"]:
            for lr in [0.05, 0.01, 0.005, 0.001]:
                for weight_decay in [0, 0.01, 0.1, 0.2, 0.3, 0.4]:
                    for scheduler_step_size in [5, 10, 15, 20]:
                        for scheduler_gamma in [0.1, 0.5]:
                            n += 1
                            if model_name_o == model_name and optimizer_name_o == optimizer_name and lr == lr_o and weight_decay_o == weight_decay and scheduler_step_size_o == scheduler_step_size and scheduler_gamma == scheduler_gamma_o:
                                return n
    return None


def params_2_model_weights_path(model_name, optimizer_name, lr, weight_decay, scheduler_step_size, scheduler_gamma, epoch):
    params = (model_name, optimizer_name, lr, weight_decay, scheduler_step_size, scheduler_gamma)
    name_container = "{}_ContrastiveLoss_USPS_3_{}_200_{}_{}_{}_{}_it_{}.pt"
    return "{}_{}".format(training_nr(*params), name_container.format(*params, epoch))


class ContrastiveLossGUI(QWidget):

    __slots__ = ["params_updated", "visualization_thread", "kill_thread"]

    def update_params(self):
        for key in list(mapper.keys()):
            title = getattr(self, "{}_title".format(key))
            name = " ".join([word.capitalize() for word in key.split("_")])
            value = mapper[key][getattr(self, key).value()]
            title.setText("{}: {}".format(name, value))
        self.params_updated = True

    def update_visualization(self):
        p2m_params = list(params_2_model_weights_path.__code__.co_varnames)[0: params_2_model_weights_path.__code__.co_argcount]
        p2m_params_values = [mapper[key][getattr(self, key).value()] for key in p2m_params]
        model_weights_path = "./weights/contrastive/{}".format(params_2_model_weights_path(*p2m_params_values))
        print(model_weights_path)

        def gui_callb(model, dataloader):
            visualize(model, dataloader, show_immediately=False, save=False, fig=self.figure)
        data_type = mapper["data_type"][getattr(self, "data_type").value()]
        print(data_type)
        test_qualitatively(model_weights_path, data_type=data_type, num_samples=mapper["num_samples"][getattr(self, "num_samples").value()], gui_callb=gui_callb)
        self.canvas.draw()

    def update_visualization_loop(self):
        while True:
            if self.params_updated:
                self.params_updated = False
                self.update_visualization()

            if self.kill_thread:
                break
            time.sleep(0.1)

    def __init__(self, parent=None):
        super(ContrastiveLossGUI, self).__init__(parent)
        self.params_updated = False
        self.kill_thread = False
        sliders_layout = QVBoxLayout()

        for key in list(mapper.keys()):
            setattr(self, "{}_title".format(key), QLabel(""))
            getattr(self, "{}_title".format(key)).setAlignment(Qt.AlignLeft)
            sliders_layout.addWidget(getattr(self, "{}_title".format(key)))

            setattr(self, key, QSlider(Qt.Horizontal))
            slider = getattr(self, key)
            slider.setMinimum(0)
            slider.setMaximum(len(mapper[key]) - 1)
            slider.setTickInterval(0)
            slider.setTickPosition(QSlider.TicksBelow)
            sliders_layout.addWidget(slider)
            slider.valueChanged.connect(self.valuechange)

        self.setWindowTitle("Contrastive Loss")

        self.figure = plt.figure(figsize=(10.0, 10.0))
        self.canvas = FigureCanvasQTAgg(self.figure)

        layout = QGridLayout()
        layout.addLayout(sliders_layout, 0, 0)
        layout.addWidget(self.canvas, 0, 1)
        layout.setColumnMinimumWidth(0, 200)
        self.setLayout(layout)
        self.update_params()
        self.visualization_thread = threading.Thread(target=self.update_visualization_loop)
        self.visualization_thread.start()

    def valuechange(self):
        self.update_params()

    def closeEvent(self, event):
        self.kill_thread = True
        self.visualization_thread.join()


def main():
    app = QApplication(sys.argv)
    ex = ContrastiveLossGUI()
    ex.show()
    sys.exit(app.exec_())
    print("KT")


if __name__ == '__main__':
    main()