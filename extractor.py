import numpy as np
import torch
from torch.utils.data import DataLoader
import torchvision
from net import get_model
from usps import get_USPS_data
from dataset import DatasetUSPSSingle

def dimensionality_reduction(model, num_data, num_classes, dataloader):
    x_reduced = np.zeros((num_data, num_classes), dtype=np.float)
    c = 0
    for i, samples in enumerate(dataloader):
        x_original_batch = samples[0]
        y_batch = samples[1]
        with torch.set_grad_enabled(False):
            x_batch = model(x_original_batch)
            x_reduced[c: c + x_batch.shape[0]] = x_batch.cpu().numpy()
            c += x_batch.shape[0]
    return x_reduced


def extract(model_weights_path, x=None, y=None, to_cuda=False):
    model_name = model_weights_path.split("/")[-1].split("_")[1]
    dataset_name = model_weights_path.split("/")[-1].split("_")[3]
    num_classes = int(model_weights_path.split("/")[-1].split("_")[4])

    model = get_model(model_name, num_classes)
    model.load_state_dict(torch.load(model_weights_path, map_location="cpu"))
    if to_cuda and torch.cuda.is_available():
        model.to(torch.device("cuda:0"))
    model.eval()

    if x is None and y is None:
        x, y = get_USPS_data("train")  # ili "test"
    up_sample = "SimpleNet" not in model_name
    dataset = DatasetUSPSSingle(x, y, up_sample=up_sample, to_cuda=to_cuda)
    dataloader = DataLoader(dataset, batch_size=64, shuffle=False, num_workers=0)

    # dimensionality reduction
    x_output = dimensionality_reduction(model, len(dataset), num_classes, dataloader)
    return x_output


if __name__ == '__main__':

    model_weights_path = "./weights/18_LeNet5_MMDSLoss_USPS_20_Adam_100_0.01_0.1_10_0.1_it_60.pt"
    x_output = extract(model_weights_path)
    print(x_output.shape)

