import torch
import torch.nn as nn
import torch.nn.functional as F


class ContrastiveLoss(nn.Module):
    __slots__ = ["m", "eps", "mean"]

    def __init__(self, m=1.0, eps=1e-9, mean=True):
        super(ContrastiveLoss, self).__init__()
        self.m = m
        self.eps = eps
        self.mean = mean

    def forward(self, x_i, x_j, y_i, y_j):
        distances = (x_j - x_i).pow(2).sum(1, keepdim=True)

        l1 = (y_i == y_j).type(distances.dtype)
        l2 = (y_i != y_j).type(distances.dtype)

        losses = l1 * 0.5 * distances + l2 * 0.5 * F.relu(self.m - (distances + self.eps).sqrt()).pow(2)

        return losses.mean() if self.mean else losses.sum()


class ContrastiveLossSingle(nn.Module):

    def __init__(self, m=1.0, eps=1e-9, mean=True):
        super(ContrastiveLossSingle, self).__init__()
        self.m = m
        self.eps = eps
        self.mean = mean

    def forward(self, x, y):
        distances = torch.pdist(torch.flatten(x, 1))
        l1 = (torch.pdist(y) == 0).type(x.dtype)
        l2 = 1.0 - l1

        losses = l1 * 0.5 * distances + l2 * 0.5 * F.relu(self.m - (distances + self.eps).sqrt()).pow(2)

        return losses.mean() if self.mean else losses.sum()


class MMDSLoss(nn.Module):

    def __init__(self):
        super(MMDSLoss, self).__init__()

    def forward(self, x_i_original, x_j_original, x_i, x_j):
        dx = (x_i_original - x_j_original).pow(2.0).sum(dim=(2, 3)).sqrt()
        dy = (x_i - x_j).pow(2.0).sum(dim=1, keepdim=True).sqrt()
        losses = (dx - dy).pow(2.0)
        return losses.mean()


class MMDSLossSingle(nn.Module):

    def __init__(self):
        super(MMDSLossSingle, self).__init__()

    def forward(self, x_original, x):
        dx = torch.pdist(torch.flatten(x_original, 1))
        dy = torch.pdist(torch.flatten(x, 1))
        losses = (dx - dy).pow(2.0)
        return losses.mean()


if __name__ == '__main__':
    l1 = ContrastiveLoss()
    l2 = MMDSLoss()
    print(type(l2).__name__)
