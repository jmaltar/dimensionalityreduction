import numpy as np
import h5py


def random_subset(x, y, n):
    random_indices = np.random.choice(x.shape[0], n, replace=False)
    return x[random_indices, :], y.reshape((y.shape[0], 1))[random_indices, :]


def get_USPS_data(data_type="train", num_samples_subset=None):
    with h5py.File('./data/usps/usps.h5', 'r') as hf:
        data = hf.get(data_type)
        x = data.get("data")[:]
        y = data.get("target")[:]
        y = y.reshape((y.shape[0], 1))

    if num_samples_subset is not None:
        n = x.shape[0]
        x, y = random_subset(x, y, n)
        x = x[:num_samples_subset, ]
        y = y[:num_samples_subset, ]

    return x, y



if __name__ == "__main__":
    x, y = get_USPS_data("train")
    print(x.shape)
