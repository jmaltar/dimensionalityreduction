import numpy as np
import os
from scipy.spatial.distance import pdist, squareform
from sklearn.metrics import euclidean_distances
from sklearn.manifold import MDS
from usps import get_USPS_data
from extractor import extract

def mmds_sklearn(x, d, x_init=None):
    D = euclidean_distances(x)
    if x_init is None:
        x_init = np.random.rand(x.shape[0], d)

    mds = MDS(d, max_iter=1000, n_init=1, dissimilarity="precomputed", verbose=0, eps=1e-12)
    x_output = mds.fit_transform(D, init=x_init)
    return x_output


def loss_simple(x, x_output):
    D = euclidean_distances(x)
    D_output = euclidean_distances(x_output)
    loss = np.power(D - D_output, 2.0).sum()
    return loss


def get_weights(weights_path):
    weights = sorted(list(os.listdir(weights_path)), key=lambda x: int(x.split("_")[0]))

    d_weights = {}
    for weight in weights:
        experiment_num = int(weight.split("_")[0])
        d_weights.setdefault(experiment_num, []).append(weight)

    for k, v in d_weights.items():
        d_weights[k] = sorted(v, key=lambda x: int(x.split("_")[-1].split(".")[0]))

    weights = []
    for k, v in d_weights.items():
        weights.extend(v)

    return weights


def get_mmds_sklearn(x, num_classes, data_type="train", dataset_name="USPS", directory="./results/mmds/"):
    name = "{}_{}_{}.npy".format(dataset_name.lower(), data_type, num_classes)
    if name not in list(os.listdir(directory)):
        x_output = mmds_sklearn(x, num_classes)
        np.save(directory + name, x_output)
        return x_output
    else:
        x_output = np.load(directory + name)
        return x_output


def save_to_file():
    data_type = "train"
    x, y = get_USPS_data(data_type=data_type)
    weights_path = "./weights/"
    weights = get_weights(weights_path)
    num_classes_list = list(range(20, 45, 5))
    x_output_sklearn_d = {}
    for num_classes in num_classes_list:
        print("Calculating Sklearn mMDS for {} classes".format(num_classes))
        x_output_sklearn_d[num_classes] = get_mmds_sklearn(x, num_classes, data_type=data_type)

    with open('./results_{}.out'.format(data_type), 'a') as the_file:
        for filename in weights:
            num_classes = int(filename.split("_")[4])
            model_weights_path = "{}{}".format(weights_path, filename)
            x_output_pytorch = extract(model_weights_path, x, y, to_cuda=True)
            x_output_sklearn = x_output_sklearn_d[num_classes]
            l1 = loss_simple(x, x_output_pytorch)
            l2 = loss_simple(x, x_output_sklearn)
            info_str = "Loss (PyTorch, {}): {}; Loss (Sklearn): {}".format(filename, l1, l2)
            print(info_str)
            the_file.write(info_str + "\n")


if __name__ == '__main__':

    data_type = "train"
    x, y = get_USPS_data(data_type=data_type)
    x_output_sklearn = get_mmds_sklearn(x, 20)
    #filename = "2882_SimpleNet3_MMDSLoss_USPS_20_Adam_1000_0.005_0_10_0.5_it_1000.pt"
    root = "./weights/mmds/"
    for filename in os.listdir(root):
        x_output_pytorch = extract(root + filename, x, y, to_cuda=True)
        l1 = loss_simple(x, x_output_pytorch)
        l2 = loss_simple(x, x_output_sklearn)
        info_str = "Loss (PyTorch, {}): {}; Loss (Sklearn): {}".format(filename, l1, l2)
        print(info_str)





