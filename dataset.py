import math
import numpy as np
import matplotlib.pyplot as plt
from usps import get_USPS_data
import torchvision.datasets as datasets
import torchvision.transforms as transforms

import torch
import torch.nn as nn
from torch.utils.data import Dataset


def visualize_pair(sample):
    x_i = sample[0]
    x_j = sample[1]
    p_img = np.concatenate((x_i.reshape(x_i.shape[1:3]).cpu(), x_j.reshape(x_j.shape[1:3]).cpu()), axis=1)
    plt.imshow(p_img, 'gray')
    plt.show()


class DatasetUSPSSingle(Dataset):
    __slots__ = ["N", "x", "y"]

    def __init__(self, x, y, up_sample=True, to_cuda=False):
        self.N = int(x.shape[0])
        self.x = torch.from_numpy(x).reshape((x.shape[0], 1, 16, 16))
        self.y = torch.from_numpy(y).reshape((y.shape[0], 1)).type(self.x.dtype)

        if up_sample:
            u = nn.Upsample(scale_factor=2, mode="nearest")
            self.x = u(self.x)

        if to_cuda and torch.cuda.is_available():
            self.x = self.x.to(torch.device("cuda:0"))
            self.y = self.y.to(torch.device("cuda:0"))

    def __len__(self):
        return self.N

    def __getitem__(self, item):
        return self.x[item], self.y[item]


class DatasetUSPS(Dataset):
    __slots__ = ["n", "N", "x", "y"]

    @staticmethod
    def item_to_i_j(item, n):
        k = int(math.ceil(-((3 - 2 * n) / 2 + math.sqrt(((3 - 2 * n) / 2) ** 2 + 2 * (n - 2 - item)))))
        delta = item - k * n + k * (k + 1) / 2
        j = int(k + 1 + delta)
        return k, j

    def __init__(self, x, y, up_sample=True, to_cuda=True, three_dim=False):
        # number of original samples
        self.N = int(x.shape[0])
        # number of pair samples
        self.n = int(x.shape[0] * (x.shape[0] - 1) / 2)
        self.x = torch.from_numpy(x).reshape((x.shape[0], 1, 16, 16))
        if three_dim:
            self.x = self.x.repeat(1, 3, 1, 1)

        self.y = torch.from_numpy(y)

        if up_sample:
            u = nn.Upsample(scale_factor=2, mode="nearest")
            self.x = u(self.x)

        if to_cuda and torch.cuda.is_available():
            self.x = self.x.to(torch.device("cuda:0"))
            self.y = self.y.to(torch.device("cuda:0"))

    def __len__(self):
        return self.n

    def __getitem__(self, item):
        i, j = DatasetUSPS.item_to_i_j(item, self.N)
        return self.x[i], self.x[j], self.y[i], self.y[j]


if __name__ == '__main__':
    set = datasets.MNIST("./data/mnist/test/", download=True, train=False, transform=transforms.ToTensor())

    print(set[0])






