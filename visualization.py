import matplotlib.pyplot as plt
import torch

def color(i):
    colors = [
        "#b80877",
        "#ffb547",
        "#606d7a",
        "#483D8B",
        "#87CEEB",
        "#379c63",
        "#ff5e00",
        "#e20e0e",
        "#1180ee",
        "#ff34a0",
    ]
    return colors[int(i)]


def visualize(model, dataloader, show_immediately=True, save=False, save_dest="", to_svg=False, fig=None):
    model.eval()

    if fig is None:
        fig = plt.figure()

    ax = fig.add_subplot(111, projection='3d')
    for i, samples in enumerate(dataloader):
        if i == 1000:
            break
        x_batch = samples[0]
        y_batch = samples[1]
        with torch.set_grad_enabled(False):
            output_x_batch = model(x_batch).numpy()
            label_it = y_batch.numpy()
            ax.scatter(output_x_batch[:, 0], output_x_batch[:, 1], output_x_batch[:, 2], color=[color(l) for l in label_it], alpha=0.5)

    labels = [str(i) for i in range(10)]
    handles = []
    for i, label in enumerate(labels):
        handles.append(plt.plot([], [], color=color(i), label=labels[i], marker="o", ls="")[0])

    plt.legend(handles=handles, loc="best")
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    ax.set_xlim(-1.5, 1.5)
    ax.set_ylim(-1.5, 1.5)
    ax.set_zlim(-1.5, 1.5)


    if save:
        plt.savefig(save_dest, format="svg" if to_svg else "png", dpi=300)

    if show_immediately:
        plt.show()