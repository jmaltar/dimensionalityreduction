import torch
import torch.nn as nn
import torchvision
import torch.nn.functional as F


class SimpleNet(nn.Module):
    def __init__(self, num_classes):
        self.num_classes = num_classes
        super(SimpleNet, self).__init__()
        self.classifier = nn.Sequential(
            # 16 x 16 = 256
            nn.Linear(in_features=256, out_features=128),
            nn.BatchNorm1d(128),
            nn.ReLU(), # ReLU
            nn.Linear(in_features=128, out_features=num_classes)
        )

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x


class SimpleNet2(nn.Module):
    def __init__(self, num_classes):
        self.num_classes = num_classes
        super(SimpleNet2, self).__init__()
        self.classifier = nn.Sequential(
            nn.Linear(in_features=256, out_features=256),
            nn.BatchNorm1d(256),
            nn.Sigmoid(),
            nn.Linear(in_features=256, out_features=num_classes)
        )

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x


class SimpleNet3(nn.Module):
    def __init__(self, num_classes):
        self.num_classes = num_classes
        super(SimpleNet3, self).__init__()
        self.classifier = nn.Sequential(
            nn.Linear(in_features=256, out_features=256),
            nn.BatchNorm1d(256),
            nn.Tanh(),
            nn.Linear(in_features=256, out_features=num_classes)
        )

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x


class LeNet5(nn.Module):

    def __init__(self, num_classes):
        super(LeNet5, self).__init__()

        self.feature_extractor = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5, stride=1),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2),
            nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5, stride=1),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2),
            nn.Conv2d(in_channels=16, out_channels=120, kernel_size=5, stride=1),
            nn.Tanh()
        )

        self.classifier = nn.Sequential(
            nn.Linear(in_features=120, out_features=84),
            nn.Tanh(),
            nn.Linear(in_features=84, out_features=num_classes),
        )

    def forward(self, x):
        x = self.feature_extractor(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x


def get_model(model_name, num_classes):
    model = None
    if model_name == "LeNet5":
        model = LeNet5(num_classes)
    elif model_name == "ResNet18":
        model = torchvision.models.resnet18(pretrained=False, num_classes=num_classes)
        model.conv1 = torch.nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
    elif model_name == "SimpleNet":
        model = SimpleNet(num_classes)
    elif model_name == "SimpleNet2":
        model = SimpleNet2(num_classes)
    elif model_name == "SimpleNet3":
        model = SimpleNet3(num_classes)
    return model


if __name__ == "__main__":
    net = SimpleNet(20)
    torch.save(net.state_dict(), "./test5.pt")